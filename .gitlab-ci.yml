image: registry.gitlab.com/chromaway/core-tools/chromia-images/maven-docker-java21:1.0.1

.git_template: &git_setup |
  git remote set-url --push origin "https://oauth2:${BUILD_USER_GITLAB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
  git config user.name "build-user core-dev"
  git config user.email build_user@chromaway.com

.setup:
  artifacts:
    when: always
    paths:
      - "**/*.log"
    reports:
      junit:
        - "*/target/surefire-reports/TEST-*.xml"
        - "*/target/failsafe-reports/TEST-*.xml"
    expire_in: 1 week

.release:
  stage: release
  when: manual
  before_script:
    - set -eu
    - *git_setup
    - git checkout $CI_COMMIT_REF_NAME
    - CURRENT_VERSION=$(git describe --tags --abbrev=0)
    - IFS=. read -r MAJOR MINOR PATCH <<< "$CURRENT_VERSION"

stages:
  - build
  - update-dependencies
  - release
  - deploy

build:
  extends: .setup
  stage: build
  interruptible: true
  except:
    - tags
    - /^release\/.*$/
  script:
    - mvn $MAVEN_CLI_OPTS verify

update-dependencies:
  variables:
    VERSIONS: rell.codegen.version,rell.version,postchain.version
  stage: update-dependencies
  script:
    - *git_setup
    - git checkout $CI_COMMIT_REF_NAME
    - mvn versions:update-properties -DincludeProperties=$VERSIONS
    - |
      if [[ `git status --porcelain --untracked-files=no` ]]; then
        branch_name="bot-update-versions-$(date +%s)"
        git checkout -b "$branch_name"
        git commit -am "Update version: $VERSIONS"
        if ! mvn $MAVEN_CI_OPTS verify; then
          echo "Maven build failed. Creating merge request"
          git push origin "$branch_name" -o merge_request.create -o merge_request.target=dev
        else
          echo "Maven build succeeded. Pushing changes directly to 'dev' branch."
          git push origin HEAD:dev
        fi
      fi
  only:
    - dev
  when: manual

release-minor:
  extends: .release
  script:
    - git tag $MAJOR.$((MINOR + 1)).0
    - git push --tags
  only:
    - dev

release-patch:
  extends: .release
  script:
    - git tag $MAJOR.$MINOR.$((PATCH + 1))
    - git push --tags
  only:
    - dev
    - /^support\/.*$/

deploy:
  extends: .setup
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+$/
  script:
    - mvn $MAVEN_CLI_OPTS deploy -Drevision=$CI_COMMIT_TAG

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true -s .gitlab-settings.xml"
  # Configure postgres service (https://hub.docker.com/_/postgres/)

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  paths:
    - .m2/repository
