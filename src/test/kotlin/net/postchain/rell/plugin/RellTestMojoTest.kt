package net.postchain.rell.plugin

import org.apache.maven.plugin.MojoFailureException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.io.TempDir
import org.redundent.kotlin.xml.Node
import org.redundent.kotlin.xml.TextElement
import org.redundent.kotlin.xml.parse
import java.io.File
import kotlin.test.assertEquals

class RellTestMojoTest {
    @Test
    fun testPassing(@TempDir testReports: File) {
        val mojo = RellTestMojo()
        mojo.source = File("src/test/resources/rell-test-passing/src")
        mojo.testReports = testReports
        mojo.testModule = "passing"
        mojo.execute()

        val testReport = parse(File(testReports, "rell-unit-tests.xml"))
        assertEquals("testsuite", testReport.nodeName)
        assertEquals("rell-unit-tests", testReport.attributes["name"])
        val case = testReport.children.filterIsInstance<Node>().first()
        assertEquals("testcase", case.nodeName)
        assertEquals("test_passing", case.attributes["name"])
    }

    @Test
    fun testNotPassing(@TempDir testReports: File) {
        val mojo = RellTestMojo()
        mojo.source = File("src/test/resources/rell-test-failure/src")
        mojo.testReports = testReports
        mojo.testModule = "failure"
        assertThrows<MojoFailureException> {
            mojo.execute()
        }

        val testReport = parse(File(testReports, "rell-unit-tests.xml"))
        assertEquals("testsuite", testReport.nodeName)
        assertEquals("rell-unit-tests", testReport.attributes["name"])
        val case = testReport.children.filterIsInstance<Node>().first()
        assertEquals("testcase", case.nodeName)
        assertEquals("test_failure", case.attributes["name"])
        val failure = case.children.filterIsInstance<Node>().first()
        assertEquals("failure", failure.nodeName)
        assertEquals("System function 'rell.test.assert_equals': expected <2> but was <1>", failure.attributes["message"])
    }

    @Test
    fun testNotCompiling(@TempDir testReports: File) {
        val mojo = RellTestMojo()
        mojo.source = File("src/test/resources/rell-test-not-compiling/src")
        mojo.testReports = testReports
        mojo.testModule = "notcompile"
        assertThrows<MojoFailureException> {
            mojo.execute()
        }
    }

    @Test
    fun testsAreCompiledWithSkipTests(@TempDir testReports: File) {
        val mojo = RellTestMojo()
        mojo.skipTests = true
        mojo.source = File("src/test/resources/rell-test-not-compiling/src")
        mojo.testReports = testReports
        mojo.testModule = "notcompile"
        assertThrows<MojoFailureException> {
            mojo.execute()
        }
    }

    @Test
    fun testsAreSkippedWithSkipTests(@TempDir testReports: File) {
        val mojo = RellTestMojo()
        mojo.skipTests = true
        mojo.source = File("src/test/resources/rell-test-failure/src")
        mojo.testReports = testReports
        mojo.testModule = "failure"
        mojo.execute()
    }

    @Test
    fun testsAreNotCompiledWithSkip(@TempDir testReports: File) {
        val mojo = RellTestMojo()
        mojo.skip = true
        mojo.source = File("src/test/resources/rell-test-not-compiling/src")
        mojo.testReports = testReports
        mojo.testModule = "notcompile"
        mojo.execute()
    }

    @Test
    fun testsAreSkippedCompiledWithSkip(@TempDir testReports: File) {
        val mojo = RellTestMojo()
        mojo.skip = true
        mojo.source = File("src/test/resources/rell-test-not-compiling/src")
        mojo.testReports = testReports
        mojo.testModule = "failure"
        mojo.execute()
    }
}
