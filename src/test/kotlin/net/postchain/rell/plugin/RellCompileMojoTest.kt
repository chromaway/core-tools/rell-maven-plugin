package net.postchain.rell.plugin

import assertk.assertThat
import assertk.assertions.isEqualTo
import java.io.File
import java.nio.file.Path
import kotlin.io.path.exists
import kotlin.io.path.pathString
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir

class RellCompileMojoTest {

    @Test
    fun `compile simple dapp`(@TempDir testDir: Path) {
        val mojo = RellCompileMojo()
        mojo.rootFolder = File("src/test/resources/rell-simple-dapp/")
        mojo.configFile = "chromia.yml"
        mojo.target = testDir.pathString
        mojo.execute()
        assertThat(testDir.resolve("foo.xml").exists()).isEqualTo(true)
    }
}
