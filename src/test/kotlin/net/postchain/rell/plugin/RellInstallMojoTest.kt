package net.postchain.rell.plugin

import assertk.assertThat
import assertk.assertions.isEqualTo
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.exists
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import kotlin.io.path.pathString

class RellInstallMojoTest {

    @Test
    fun `install library`(@TempDir testDir: Path) {
        val mojo = RellInstallMojo()
        mojo.configPath = "src/test/resources/rell-simple-lib-dapp/chromia.yml"
        mojo.source = testDir.pathString
        mojo.target = testDir.pathString
        mojo.execute()

        val ft4 = testDir.resolve("lib/ft4")
        assertThat(ft4.exists()).isEqualTo(true)
    }
}
