package net.postchain.rell.plugin

import org.apache.maven.project.MavenProject
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class RellGenMojoTest {

    @Test
    fun testPassing(@TempDir dir: File) {
        val mojo = RellGenMojo()
        mojo.source = File("src/test/resources/partially-named-tuple")
        mojo.target = dir
        mojo.project = MavenProject()
        mojo.force = true
        mojo.packageName = "my.package"
        mojo.execute()
    }

}