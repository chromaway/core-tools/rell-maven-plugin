package net.postchain.rell.plugin

import org.apache.maven.plugin.MojoFailureException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.io.TempDir
import org.redundent.kotlin.xml.Node
import org.redundent.kotlin.xml.parse
import java.io.File
import kotlin.test.assertEquals

class RellIntegrationTestMojoTest {
    @Test
    fun testPassing(@TempDir testReports: File) {
        val mojo = TestRellIntegrationTestMojo()
        mojo.configPath = File("src/test/resources/rell-test-passing/chromia.yml")
        mojo.testReports = testReports
        mojo.execute()

        val testReport = parse(File(testReports, "rell-integration-tests.xml"))
        assertEquals("testsuite", testReport.nodeName)
        assertEquals("rell-integration-tests", testReport.attributes["name"])
        val case = testReport.children.filterIsInstance<Node>().first()
        assertEquals("testcase", case.nodeName)
        assertEquals("test_passing", case.attributes["name"])
    }

    @Test
    fun testNotPassing(@TempDir testReports: File) {
        val mojo = TestRellIntegrationTestMojo()
        mojo.configPath = File("src/test/resources/rell-test-failure/chromia.yml")
        mojo.testReports = testReports
        assertThrows<MojoFailureException> {
            mojo.execute()
        }

        val testReport = parse(File(testReports, "rell-integration-tests.xml"))
        assertEquals("testsuite", testReport.nodeName)
        assertEquals("rell-integration-tests", testReport.attributes["name"])
        val case = testReport.children.filterIsInstance<Node>().first()
        assertEquals("testcase", case.nodeName)
        assertEquals("test_failure", case.attributes["name"])
        val failure = case.children.filterIsInstance<Node>().first()
        assertEquals("failure", failure.nodeName)
        assertEquals("System function 'rell.test.assert_equals': expected <2> but was <1>", failure.attributes["message"])
    }

    @Test
    fun testNotCompiling(@TempDir testReports: File) {
        val mojo = TestRellIntegrationTestMojo()
        mojo.configPath = File("src/test/resources/rell-test-not-compiling/chromia.yml")
        mojo.testReports = testReports
        assertThrows<MojoFailureException> {
            mojo.execute()
        }
    }

    @Test
    fun testsAreCompiledWithSkipTests(@TempDir testReports: File) {
        val mojo = RellIntegrationTestMojo()
        mojo.skipTests = true
        mojo.configPath = File("src/test/resources/rell-test-not-compiling/chromia.yml")
        mojo.testReports = testReports
        assertThrows<MojoFailureException> {
            mojo.execute()
        }
    }

    @Test
    fun testsAreSkippedWithSkipTests(@TempDir testReports: File) {
        val mojo = RellIntegrationTestMojo()
        mojo.skipTests = true
        mojo.configPath = File("src/test/resources/rell-test-failure/chromia.yml")
        mojo.testReports = testReports
        mojo.execute()
    }

    @Test
    fun testsAreNotCompiledWithSkip(@TempDir testReports: File) {
        val mojo = RellIntegrationTestMojo()
        mojo.skip = true
        mojo.configPath = File("src/test/resources/rell-test-not-compiling/chromia.yml")
        mojo.testReports = testReports
        mojo.execute()
    }

    @Test
    fun testsAreSkippedCompiledWithSkip(@TempDir testReports: File) {
        val mojo = RellIntegrationTestMojo()
        mojo.skip = true
        mojo.configPath = File("src/test/resources/rell-test-not-compiling/chromia.yml")
        mojo.testReports = testReports
        mojo.execute()
    }
}

internal class TestRellIntegrationTestMojo : RellIntegrationTestMojo() {
    override fun fetchDbUrl() = null
}
