package net.postchain.rell.plugin

import com.chromia.api.ChromiaLibrariesApi
import com.chromia.build.tools.lib.GitRepositoryCloner
import com.chromia.cli.model.parseModel
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.apache.maven.project.MavenProject
import kotlin.io.path.Path

@Mojo(name = "rell-install", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
class RellInstallMojo : AbstractMojo() {
    @Parameter(defaultValue = "\${project}")
    lateinit var project: MavenProject

    /**
     * Skip plug-in execution.
     */
    @Parameter(property = "skip")
    var skip = false

    @Parameter(name = "source", defaultValue = "\${basedir}/rell/src")
    lateinit var source: String

    @Parameter(name = "target", defaultValue = "\${project.build.directory}/rell")
    lateinit var target: String

    @Parameter(name = "configPath", defaultValue = "\${basedir}/rell/chromia.yml")
    lateinit var configPath: String

    @Throws(MojoFailureException::class)
    override fun execute() {
        if (skip) {
            log.info("Skipping execution...")
            return
        }
        var model = parseModel(Path(configPath))

        source.apply {
            model = model.copy(compile = model.compile.copy(source = Path(this)))
        }
        target.apply {
            model = model.copy(compile = model.compile.copy(target = Path(this)))
        }

        ChromiaLibrariesApi.install(MavenRellCliEnv(log), model, GitRepositoryCloner())
        log.info("Installed libraries: ${model.libs.keys}")
    }
}
