package net.postchain.rell.plugin

import com.chromia.api.ChromiaCompileApi
import com.chromia.cli.model.parseModel
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.apache.maven.project.MavenProject
import java.io.File
import kotlin.io.path.Path

@Mojo(name = "rell-compile", defaultPhase = LifecyclePhase.COMPILE)
class RellCompileMojo : AbstractMojo() {
    @Parameter(defaultValue = "\${project}")
    lateinit var project: MavenProject

    /**
     * Skip plug-in execution.
     */
    @Parameter(property = "skip")
    var skip = false

    @Parameter(property = "rootFolder", defaultValue = "\${basedir}")
    lateinit var rootFolder: File

    @Parameter(name = "source")
    var source: String? = null

    @Parameter(name = "target")
    var target: String? = null

    @Parameter(name = "configFile", defaultValue = "rell/chromia.yml")
    lateinit var configFile: String

    @Throws(MojoFailureException::class)
    override fun execute() {
        if (skip) {
            log.info("Skipping execution...")
            return
        }
        val root = rootFolder.normalize().absoluteFile
        val model = parseModel(root.resolve(configFile))
        var compileModel = model.compile
        source?.apply {
            compileModel = compileModel.copy(source = Path(this))
        }
        target?.apply {
            compileModel = compileModel.copy(target = Path(this))
        }
        val resolvedModel = model.copy(compile = compileModel)

        ChromiaCompileApi.build(MavenRellCliEnv(log), resolvedModel).forEach {
            it.save(resolvedModel.compile.target)
        }
        log.info("Rell compilation complete")
    }
}
