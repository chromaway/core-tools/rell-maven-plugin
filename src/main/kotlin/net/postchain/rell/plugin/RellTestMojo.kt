package net.postchain.rell.plugin

import net.postchain.rell.api.base.RellApiCompile
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import java.io.File

@Mojo(name = "rell-test", defaultPhase = LifecyclePhase.TEST)
class RellTestMojo : AbstractRellTestMojo() {
    @Parameter(name = "source", defaultValue = "\${basedir}/rell/src")
    lateinit var source: File

    @Parameter(name = "testModule", required = true)
    lateinit var testModule: String

    @Parameter(name = "dbUrl")
    var dbUrl: String? = null

    override fun fetchCompileConfigBuilder() = RellApiCompile.Config.Builder()
            .includeTestSubModules(true)
            .appModuleInTestsError(false)
            .moduleArgsMissingError(true)
            .mountConflictError(true)

    override fun fetchDbUrl() = dbUrl

    override fun fetchSourceDir() = source

    override fun fetchTestModules(): List<String> = testModule.split(",".toRegex()).dropLastWhile { it.isEmpty() }
    override fun testReportFileName() = "rell-unit-tests"
}
