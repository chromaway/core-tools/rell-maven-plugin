package net.postchain.rell.plugin

import net.postchain.rell.api.base.RellCliEnv
import org.apache.maven.plugin.logging.Log

class MavenRellCliEnv(private val log: Log) : RellCliEnv {
    val errorCache = mutableListOf<String>()
    override fun print(msg: String) = log.info(msg)
    override fun error(msg: String) = log.error(msg).also { errorCache.add(msg) }
}
