package net.postchain.rell.plugin

import net.postchain.rell.codegen.CodeGenerator
import net.postchain.rell.codegen.StringSerializable
import net.postchain.rell.codegen.document.DocumentSaver
import net.postchain.rell.codegen.kotlin.KotlinCodeGeneratorConfig
import net.postchain.rell.codegen.kotlin.KotlinDocumentFactory
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.apache.maven.project.MavenProject
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors

@Mojo(name = "rell-generate-client", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
class RellGenMojo : AbstractMojo() {
    @Parameter(defaultValue = "\${project}")
    lateinit var project: MavenProject

    /**
     * Skip plug-in execution.
     */
    @Parameter(property = "skip")
    var skip = false

    /**
     * Force plug-in execution.
     */
    @Parameter(property = "force")
    var force = false

    @Parameter(name = "source", defaultValue = "\${basedir}/rell/src")
    lateinit var source: File

    @Parameter(name = "target", defaultValue = "\${project.build.directory}/generated-sources/rell-client")
    lateinit var target: File

    @Parameter(name = "mainModule", readonly = true)
    var mainModule: String? = null

    @Parameter(name = "mainModules", readonly = true)
    var mainModules: List<String>? = null

    @Parameter(name = "packageName", readonly = true, required = true)
    lateinit var packageName: String

    @Parameter(property = "generateQueries")
    var generateQueries = true

    @Parameter(property = "generateOperations")
    var generateOperations = true

    @Throws(MojoFailureException::class)
    override fun execute() {
        if (skip) {
            log.info("Skipping execution...")
            return
        }
        if (force) {
            generate()
        } else {
            val sourceModified = latestModifiedFileInTree(source)
            log.debug("sourceModified: $sourceModified")
            val targetModified = latestModifiedFileInTree(target)
            log.debug("targetModified: $targetModified")
            if (targetModified > sourceModified) {
                log.info("Nothing to do - all files are up to date")
            } else {
                generate()
            }
        }
        project.addCompileSourceRoot(target.path)
    }

    private fun latestModifiedFileInTree(directory: File?): Long {
        if (directory!!.exists()) {
            try {
                Files.walk(directory.toPath()).use { files -> return files.mapToLong { p: Path -> p.toFile().lastModified() }.max().orElse(0L) }
            } catch (e: IOException) {
                log.error("Unable to traverse " + directory.name, e)
                return 0L
            }
        } else {
            return 0L
        }
    }

    private fun generate() {
        val config = object: KotlinCodeGeneratorConfig {
            override fun packageName() = packageName
            override fun includeOperations() = generateOperations
            override fun includeQueries() = generateQueries
        }
        val factory = KotlinDocumentFactory(config)
        val generator = CodeGenerator(factory, config, MavenRellCliEnv(log))
        val documents = HashMap<String, StringSerializable>()
        if (mainModule != null) {
            documents.putAll(generateForMainModule(generator, mainModule!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }))
        } else {
            documents.putAll(generateForMainModule(generator, mainModules))
        }
        DocumentSaver(target).saveDocuments(documents)
        log.info("Created " + documents.size + " files")
    }

    private fun generateForMainModule(generator: CodeGenerator, mainModules: List<String>?): Map<String, StringSerializable> {
        val sections = generator.createSections(source, mainModules)
        val documents = generator.constructDocuments(sections)
        log.info("Main module " + mainModule + " created files: " + documents.keys.stream().collect(Collectors.joining(", ", "[", "]")))
        return documents
    }
}
