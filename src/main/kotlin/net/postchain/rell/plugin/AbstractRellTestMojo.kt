package net.postchain.rell.plugin

import com.chromia.build.tools.test.xmlTestReport
import net.postchain.rell.api.base.RellApiCompile
import net.postchain.rell.api.base.RellCliException
import net.postchain.rell.api.gtx.RellApiRunTests
import net.postchain.rell.base.runtime.Rt_Exception
import net.postchain.rell.base.runtime.Rt_Printer
import net.postchain.rell.base.runtime.utils.Rt_Utils
import net.postchain.rell.base.utils.UnitTestCase
import net.postchain.rell.base.utils.UnitTestCaseResult
import net.postchain.rell.base.utils.UnitTestRunnerResults
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoFailureException
import org.apache.maven.plugins.annotations.Parameter
import java.io.File
import java.nio.file.Files

abstract class AbstractRellTestMojo : AbstractMojo() {
    /**
     * Skip plug-in execution.
     */
    @Parameter(property = "skip")
    var skip = false

    /**
     * Skip running tests.
     */
    @Parameter(property = "skipTests")
    var skipTests = false

    @Parameter(property = "skipAfterFailureCount", defaultValue = "0")
    var skipAfterFailureCount = 0

    @Parameter(name = "rell.test")
    var tests: String? = null

    @Parameter(name = "testReports", defaultValue = "\${project.build.directory}/rell-reports")
    lateinit var testReports: File

    @Throws(MojoFailureException::class)
    override fun execute() {
        if (skip) {
            log.info("Tests are skipped.")
            return
        }

        if (skipTests) {
            log.info("Tests are compiled, but not run.")
        }

        val printer = object : Rt_Printer {
            override fun print(str: String) = log.info(str)
        }
        val cliEnv = MavenRellCliEnv(log)
        val compileConfig = fetchCompileConfigBuilder()
                .cliEnv(cliEnv)
                .build()
        val testConfig = RellApiRunTests.Config.Builder()
                .compileConfig(compileConfig)
                .testPatterns(if (skipTests) listOf("") else tests?.split(","))
                .databaseUrl(if (skipTests) null else fetchDbUrl())
                .sqlLog(false)
                .logPrinter(printer)
                .outPrinter(printer)
                .stopOnError(skipAfterFailureCount > 0)
                .printTestCases(false)
                .onTestCaseStart { case -> case.print() }
                .onTestCaseFinished { res -> res.print() }
                .build()
        try {
            val res = RellApiRunTests.runTests(testConfig, fetchSourceDir(), listOf(), fetchTestModules())
            generateReport(res)
            printResults(res)
        } catch (e: RellCliException) {
            throw MojoFailureException(e.message + ":\n" + cliEnv.errorCache.joinToString("\n"))
        }
        log.info("Test execution complete")
    }

    abstract fun fetchCompileConfigBuilder(): RellApiCompile.Config.Builder
    abstract fun fetchDbUrl(): String?
    abstract fun fetchSourceDir(): File
    abstract fun fetchTestModules(): List<String>
    abstract fun testReportFileName(): String

    private fun UnitTestCase.print() {
        log.info("Starting test $name")
    }

    private fun UnitTestCaseResult.print() {
        if (res.isOk) log.info("OK $case")
        else {
            val message = when (val exception = res.error) {
                is Rt_Exception -> Rt_Utils.appendStackTrace("${exception.message}", exception.info.stack)
                is Throwable -> "${exception.message}"
                else -> "FAILED"
            }
            log.error("ERROR $case: $message")
        }
    }

    private fun generateReport(res: UnitTestRunnerResults) {
        testReports.mkdirs()
        Files.writeString(testReports.toPath().resolve("${testReportFileName()}.xml"), res.xmlTestReport(testReportFileName()))
    }

    private fun printResults(res: UnitTestRunnerResults) {
        if (res.getResults().any { !it.res.isOk }) {
            throw MojoFailureException("TEST FAILURE")
        }
    }
}
