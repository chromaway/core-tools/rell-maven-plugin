package net.postchain.rell.plugin

import com.chromia.cli.model.parseModel
import net.postchain.rell.api.base.RellApiCompile
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import java.io.File
import java.nio.file.Path

@Mojo(name = "rell-integration-test", defaultPhase = LifecyclePhase.INTEGRATION_TEST)
open class RellIntegrationTestMojo : AbstractRellTestMojo() {
    @Parameter(name = "configPath", defaultValue = "\${basedir}/rell/chromia.yml")
    lateinit var configPath: File

    private val settings by lazy { parseModel(configPath) }

    override fun fetchCompileConfigBuilder() = RellApiCompile.Config.Builder()
            .moduleArgs(settings.test.moduleArgs)
            .includeTestSubModules(true)
            .appModuleInTestsError(false)
            .moduleArgsMissingError(true)
            .mountConflictError(true)
            .version(settings.compile.langVersion)

    override fun fetchDbUrl(): String? = settings.databaseUrl

    override fun fetchSourceDir(): File = settings.compile.source.toFile()

    override fun fetchTestModules() = settings.test.modules
    override fun testReportFileName() = "rell-integration-tests"
}
